const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController');
const auth = require('../auth');


// Non-admin User checkout(Create Order)

router.post("/createOrder", auth.verify, (req, res) => {

const userData = auth.decode(req.headers.authorization)

  	orderController.createOrder(req.body, {userId: userData.id, isAdmin: userData.isAdmin}).then(resultFromController => res.send(resultFromController))
});



module.exports = router; 




// stretch goal

// retrieve authenticated user's order

/*router.get("/myOrder/:userId", auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization)
	
		orderController.getMyOrder({id: userData.id}).then(resultFromController => res.send(resultFromController))
});*/

// add to cart
	// added products
	// change product from cart
	// subtotal for each item
	// total price for all items
