const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');

// create product (admin only) function
module.exports.addProduct = (reqBody, userData) => {

     return User.findById(userData.userId).then(result => {
        if (userData.isAdmin == false) {
            return "Sorry, you are not an Admin"
        } else {

            let newProduct = new Product({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            });

            return newProduct.save().then((product, error) => {

                if (error) {
                    return false

                } else {
                    return "Product has been successfully added"
                }
            })
        } 
    })
};

// retrieve all active products function
module.exports.getAllActive = () => {
    return Product.find({ isActive: true }).then(result => {
        return result
    })
};

// retrieve single product function

module.exports.getProduct = (reqParams) => {
    return Product.findById(reqParams.productId).then(result => {
        return result
    })
};


// Update Product Information (Admin Only) function

module.exports.updateProduct = (reqBody, userData) => {

    return Product.findById(userData.productId).then((result, error) => {

    if(userData.isAdmin === false) {
        return "Sorry, you are not an Admin"
    } else {
        let updatedProduct = new Product({
            name: reqBody.name,
            description: reqBody.description,
            price: reqBody.price
        });


        return updatedProduct.save().then((product, error) => {

                if(error) {

                    return false;

                } else {

                    return "Product updated successfully";
                }
            })

        } 

    })
};


// Archive Product(Admin Only) function
module.exports.archiveProduct = (data) => {

    return Product.findById(data.productId).then((result, error) => {

    if(data.isAdmin === true) {

       result.isActive = false;

            return result.save().then((archivedProduct, error) => {

                if(error) {

                    return false;

                } else {

                    return "Product archived successfully";
                }
            })

        } else {
            return "Sorry, you are not an Admin"
        }
    })
};
